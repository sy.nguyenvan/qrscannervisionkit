//
//  ViewController.swift
//  QRScannerVisionKit
//
//  Created by sy.nguyenvan on 04/26/2023.
//  Copyright (c) 2023 sy.nguyenvan. All rights reserved.
//

import UIKit
import QRScannerVisionKit

class ViewController: UIViewController {
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func button(_ sender: Any) {
        let vc = ScannerVisionKitController.shared
        vc.controllerDelegate = self
        vc.showCamera(vc: self)
    }
    
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Scanner Result", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        //self.view.addSubview(alert.view)
    }

}

extension ViewController: ScannerVisionKitControllerDelegate {
    
    func removeView() {
        
    }
    
    func updateDataScan(scannerCode: String) {
        showAlert(scannerCode)
    }
}

