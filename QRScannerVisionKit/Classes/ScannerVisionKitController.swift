//
//  CusDataScannerController.swift
//  TestScannerVision
//
//  Created by Nguyen Van Sy on 25/04/2023.
//

import Foundation
import SwiftUI
import VisionKit

public protocol ScannerVisionKitControllerDelegate: AnyObject {
    func removeView()
    func updateDataScan(scannerCode: String)
}

public class ScannerVisionKitController: UIViewController {
    
    public static var shared = ScannerVisionKitController()
    public weak var controllerDelegate: (ScannerVisionKitControllerDelegate)?
    var scannerAvailable: Bool {
        DataScannerViewController.isSupported &&
        DataScannerViewController.isAvailable
    }
    
    private var cameraWidth = CGFloat(0)
    private var cameraHeight = CGFloat(0)
    private var angleWidth = CGFloat(3)
    private var angleLength = CGFloat(15)
    private var commonXOffset = CGFloat(0)
    private var commonYOffset  = CGFloat(0)
    private init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        cameraWidth = CGFloat(view.bounds.width * 3/4)
        cameraHeight = CGFloat(view.bounds.height / 3)
        commonXOffset = CGFloat(view.bounds.width * 3/8) // cameraWidth / 2
        commonYOffset  = CGFloat(view.bounds.height / 6) // cameraHeight / 2
    }
    
    public func showCamera(vc: UIViewController) {
        /// Defined viewController type of. DataScannerViewController
        let viewController = DataScannerViewController(
            recognizedDataTypes: [.barcode()],
            qualityLevel: .fast,
            recognizesMultipleItems: false,
            isHighFrameRateTrackingEnabled: false,
            isHighlightingEnabled: true)
        
        /// Call delegate func of CusDataScannerControllerDelegate
        viewController.delegate = self
        
        /// Show DataScannerViewController and start scanning data
        vc.show(viewController, sender: nil)
        try? viewController.startScanning()
        
        self.createCameraAngleBounds(viewController)
        self.createScanningIndicator(viewController)
    }
    
    func createCameraAngleBounds(_ vc: UIViewController) {
        for index in 0...8 {
            let layer = CALayer()
            let angleColor = UIColor.green
            layer.backgroundColor = angleColor.cgColor
            switch index {
            case 0:
                // left top x
                layer.frame = CGRect(x: view.bounds.width / 2 - (self.cameraWidth/2), y: view.bounds.height / 2 - (self.cameraHeight/2), width: self.angleLength, height: self.angleWidth)
            case 1:
                // left top y
                layer.frame = CGRect(x: view.bounds.width / 2 - (self.cameraWidth/2), y: view.bounds.height / 2 - (self.cameraHeight/2), width: self.angleWidth, height: self.angleLength)
            case 2:
                // right top x
                layer.frame = CGRect(x: view.bounds.width / 2 + (self.cameraWidth/2 - self.angleLength), y: view.bounds.height / 2 - (self.cameraHeight/2), width: self.angleLength, height: self.angleWidth)
            case 3:
                // right top y
                layer.frame = CGRect(x: view.bounds.width / 2 + (self.cameraWidth/2), y: view.bounds.height / 2 - (self.cameraHeight/2), width: self.angleWidth, height: self.angleLength)
            case 4:
                // left bottom x
                layer.frame = CGRect(x: view.bounds.width / 2 - (self.cameraWidth/2), y: view.bounds.height / 2 + (self.cameraHeight/2), width: self.angleLength, height: self.angleWidth)
            case 5:
                // left bottom y
                layer.frame = CGRect(x: view.bounds.width / 2 - (self.cameraWidth/2), y: view.bounds.height / 2 + (self.cameraHeight/2 - self.angleLength), width: self.angleWidth, height: self.angleLength)
            case 6:
                // right bottom x
                layer.frame = CGRect(x: view.bounds.width / 2 + (self.cameraWidth/2 - self.angleLength), y: view.bounds.height / 2 + (self.cameraHeight/2), width: self.angleLength, height: self.angleWidth)
            case 7:
                // right bottom y
                layer.frame = CGRect(x: view.bounds.width / 2 + (self.cameraWidth/2), y: view.bounds.height / 2 + (self.cameraHeight/2 - self.angleLength), width: self.angleWidth, height: self.angleLength + self.angleWidth)
            default:
                return
            }
            vc.view.layer.addSublayer(layer)
        }
    }

    func createScanningIndicator(_ vc: UIViewController) {
        let height: CGFloat = 10
        let opacity: Float = 0.4
        let topColor = UIColor.green.withAlphaComponent(0)
        let bottomColor = UIColor.green

        let layer = CAGradientLayer()
        layer.colors = [topColor.cgColor, bottomColor.cgColor]
        layer.opacity = opacity
        
        let xOffset = view.bounds.width / 2 - (self.cameraWidth/2)
        let yOffset = view.bounds.height / 2 - (self.cameraHeight/2 + 10)
        layer.frame = CGRect(x: xOffset, y: yOffset, width: self.cameraWidth, height: height)

        let initialYPosition = layer.position.y
        let finalYPosition = initialYPosition + self.cameraHeight
        let duration: CFTimeInterval = 2

        let animation = CABasicAnimation(keyPath: "position.y")
        animation.fromValue = initialYPosition as NSNumber
        animation.toValue = finalYPosition as NSNumber
        animation.duration = duration
        animation.repeatCount = .infinity
        animation.isRemovedOnCompletion = false
        
        layer.add(animation, forKey: nil)
        vc.view.layer.addSublayer(layer)
    }
}

extension ScannerVisionKitController: DataScannerViewControllerDelegate {
    public func dataScanner(_ dataScanner: DataScannerViewController, didAdd addedItems: [RecognizedItem], allItems: [RecognizedItem]) {
        for item in addedItems {
            switch item {
            case .barcode(let code):
                guard let stringResult = code.payloadStringValue else { return }
                
                /// Check if stringResult is URL return url else return string
                if  let url = URL(string: stringResult), UIApplication.shared.canOpenURL(url) {
                    //After scanning the data,stop sacnning and turn off DataScannerView
                    dataScanner.stopScanning()
                    dataScanner.dismiss(animated: false)
                    self.controllerDelegate?.removeView()
                    self.dismiss(animated: true)
                    
                    /// Update data result scanning for delegate func updateDataScan
                    self.controllerDelegate?.updateDataScan(scannerCode: stringResult)
                } else {
                    /// After scanning the data,stop sacnning and turn off DataScannerView
                    dataScanner.stopScanning()
                    dataScanner.dismiss(animated: false)
                    self.controllerDelegate?.removeView()
                    self.dismiss(animated: true)
                    
                    /// Update data result scanning for delegate func updateDataScan
                    self.controllerDelegate?.updateDataScan(scannerCode: stringResult)
                }
            default:
                /// After scanning the data,stop sacnning and turn off DataScannerView
                dataScanner.stopScanning()
                dataScanner.dismiss(animated: false)
                self.controllerDelegate?.removeView()
                self.dismiss(animated: true)
                
                /// Update data result scanning for delegate func updateDataScan
                self.controllerDelegate?.updateDataScan(scannerCode: "Result Unknown")
            }
        }
    }
    
    public func dataScannerDidZoom(_ dataScanner: DataScannerViewController) {
        
    }
    
    public func dataScanner(_ dataScanner: DataScannerViewController, didTapOn item: RecognizedItem) {
        
    }
    
    public func dataScanner(_ dataScanner: DataScannerViewController, didUpdate updatedItems: [RecognizedItem], allItems: [RecognizedItem]) {
        
    }
    
    public func dataScanner(_ dataScanner: DataScannerViewController, didRemove removedItems: [RecognizedItem], allItems: [RecognizedItem]) {
        
    }
    
    public func dataScanner(_ dataScanner: DataScannerViewController, becameUnavailableWithError error: DataScannerViewController.ScanningUnavailable) {
        
        
    }
}
