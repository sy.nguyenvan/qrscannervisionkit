# QRScannerVisionKit

[![CI Status](https://img.shields.io/travis/sy.nguyenvan/QRScannerVisionKit.svg?style=flat)](https://travis-ci.org/sy.nguyenvan/QRScannerVisionKit)
[![Version](https://img.shields.io/cocoapods/v/QRScannerVisionKit.svg?style=flat)](https://cocoapods.org/pods/QRScannerVisionKit)
[![License](https://img.shields.io/cocoapods/l/QRScannerVisionKit.svg?style=flat)](https://cocoapods.org/pods/QRScannerVisionKit)
[![Platform](https://img.shields.io/cocoapods/p/QRScannerVisionKit.svg?style=flat)](https://cocoapods.org/pods/QRScannerVisionKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QRScannerVisionKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'QRScannerVisionKit'
```

## Author

sy.nguyenvan, sy.nguyenvan@vti.com.vn

## License

QRScannerVisionKit is available under the MIT license. See the LICENSE file for more info.
