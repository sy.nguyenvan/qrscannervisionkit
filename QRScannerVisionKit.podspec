#
# Be sure to run `pod lib lint TFLiteCamera.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'QRScannerVisionKit'
  s.version          = '0.0.4'
  s.summary          = 'iOS SDK for scan qr and barcode'
  s.swift_version    = '4.0'
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'iOS SDK for scan qr and barcode, with example'

  s.homepage         = 'https://gitlab.com/sy.nguyenvan'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'sy.nguyenvan' => 'sy.nguyenvan@vti.com.vn' }
  s.source           = { :git => 'https://gitlab.com/sy.nguyenvan/qrscannervisionkit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '16.0'

  s.source_files = 'QRScannerVisionKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TFLiteCamera' => ['TFLiteCamera/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.platform = :ios, "16.0"
  s.static_framework = true
end
